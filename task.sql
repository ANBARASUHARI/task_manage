CREATE TABLE tasks (
	task_id SERIAL PRIMARY KEY,
	user_id INT NOT NULL UNIQUE,
	project_id INT NOT NULL,
	c_id INT NOT NULL,
	title VARCHAR(255) NOT NULL,
	description TEXT,
	due_date DATE,
	status VARCHAR(20) NOT NULL,
	created_date TIMESTAMP,
	updated_date TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users(user_id),
	FOREIGN KEY (project_id) REFERENCES projects(project_id),
	FOREIGN KEY (c_id) REFERENCES category(c_id)
);
