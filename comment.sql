CREATE TABLE comments (
	comm_id SERIAL PRIMARY KEY,
	task_id INT NOT NULL,
	content VARCHAR(512) NOT NULL,
	created_date TIMESTAMP,
	updated_date TIMESTAMP,
	FOREIGN KEY (task_id) REFERENCES tasks(task_id)
);
